# Ansible role: labocbz.install_node_exporter

![Licence Status](https://img.shields.io/badge/licence-MIT-brightgreen)
![Testing Method](https://img.shields.io/badge/Testing%20Method-Ansible%20Molecule-blueviolet)
![Testing Driver](https://img.shields.io/badge/Testing%20Driver-docker-blueviolet)
![Language Status](https://img.shields.io/badge/language-Ansible-red)
![Compagny](https://img.shields.io/badge/Compagny-Labo--CBZ-blue)
![Author](https://img.shields.io/badge/Author-Lord%20Robin%20Crombez-blue)

## Description

![Tag: Ansible](https://img.shields.io/badge/Tech-Ansible-orange)
![Tag: Debian](https://img.shields.io/badge/Tech-Debian-orange)
![Tag: Ubuntu](https://img.shields.io/badge/Tech-Ubuntu-orange)
![Tag: node-exporter](https://img.shields.io/badge/Tech-node--exporter-orange)
![Tag: prometheus](https://img.shields.io/badge/Tech-prometheus-orange)

An Ansible role to install and configure node-exporter service on your host.

The provided Ansible role automates the setup of Prometheus' Node Exporter, a vital tool for collecting essential system and hardware metrics crucial for comprehensive monitoring. By simplifying the intricate process of deploying Node Exporter instances, the role offers versatile customization options tailored to unique monitoring needs. It facilitates seamless installation and configuration while ensuring secure communication through SSL/TLS encryption.

Additionally, the role enables effortless metrics selection, empowering users to adapt Node Exporter to specific monitoring requirements. With provisions for fortified access control through basic authentication, the role contributes to heightened security. Ultimately, this Ansible role streamlines Node Exporter deployment, fostering efficient performance monitoring, proactive maintenance, and well-informed decision-making through its customization capabilities, security enhancements, and tailored metrics collection.

## Usage

```SHELL
# Install husky and init
npm i && npx husky init && npm i validate-branch-name && npm cz

# Initialize the local secrets database, if not already present
MSYS_NO_PATHCONV=1 docker run -it --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest detect-secrets scan --exclude-files 'node_modules' > .secrets.baseline

# Decrypt the local Ansible Vault
# Get the Ansible Local Vault Key on PassBolt and create the .ansible-vault.key file, after that you can unlock your local vault
MSYS_NO_PATHCONV=1 docker run --rm -it -v "$HOME/.docker:/root/.docker" -v "$(pwd):/root/ansible/${DIR}" -w /root/ansible/${DIR} labocbz/ansible-molecule:latest /bin/sh -c 'cat ./.ansible-vault.key > /tmp/.ansible-vault.key && chmod -x /tmp/.ansible-vault.key && ansible-vault decrypt --vault-password-file /tmp/.ansible-vault.key ./tests/inventory/group_vars/vault.yml'
```

### Linters

```SHELL
# Analyse the project for secrets, and audit
./detect-secrets

# Lint Markdown
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest markdownlint './**.md' --disable MD013

# Lint YAML
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest yamllint -c ./.yamllint .

# Lint Ansible
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest ansible-lint --offline --exclude node_modules -x meta-no-info -p .
```

### Local Tests

This repository contain a unique Molecule controller to start containers and tests your roles and playbook inside them.

```SHELL
# Start the Molecule controller
./molecule-controller

# Inside the controller
molecule create
molecule converge
molecule verify
molecule test
```

### Use

In order to use this role, you have to know its defaults vars and their purppose.

### Vars

This role have these default vars here: [defaults.yml](./defaults/main.yml).

### Import and Run

To run this role or use it inside a playbook, import the call task from [converge.yml](./molecule/default/converge.yml).

## Architectural Decisions Records

Here you can put your change to keep a trace of your work and decisions.

### 2023-08-07: First Init

* First init of this role with the bootstrap_role playbook by Lord Robin Crombez

### 2023-08-07-b: Confs, SSL/TLS, Auth

* Role can handle SSL/TLS
* Role handle loglevel and port configuration
* Crypto material have to be created and deployed before (no generation)
* Role doesn't handle the hashing of the password

### 2023-08-10: Log redirection

* Exporter lor are now redirected into a file

### 2023-10-06: New CICD, new Images

* New CI/CD scenario name
* Molecule now use remote Docker image by Lord Robin Crombez
* Molecule now use custom Docker image in CI/CD by env vars
* New CICD with needs and optimization

### 2023-12-14: System users

* Role can now use system users and address groups

### 2024-02-22: New CICD and fixes

* Added support for Ubuntu 22
* Added support for Debian 11/22
* Edited vars for linting (role name and __)
* Added generic support for Docker dind (can add used for obscures reasons ... user in use)
* Fix idempotency
* Added informations for UID and GID for user/groups
* Added support for user password creation (on_create)
* New CI, need work on tag and releases
* CI use now Sonarqube

### 2024-05-19: New CI

* Added Markdown lint to the CICD
* Rework all Docker images
* Change CICD vars convention
* New workers
* Removed all automation based on branch

### 2024-10-13: Ansible Vault and automations

* Added one local Ansible Vault
* Edited gitignore file
* Add some commands in documentation
* Refactored the role

### 2024-12-31: New CICD and images

* Edited all Docker images
* Rework on the CICD
* Enabled SonarQube

### 2025-01-05: Certificates update

* Edit for use the latest version of import_certificates

## Authors

* Lord Robin Crombez

## Sources

* [Ansible role documentation](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html)
* [Ansible Molecule documentation](https://molecule.readthedocs.io/)
